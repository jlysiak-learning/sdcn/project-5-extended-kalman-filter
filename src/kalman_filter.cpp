#include "kalman_filter.h"

#include <iostream>

static double normalizeAngle(double angle) {
  const double pi = 2 * std::acos(0.);
  if (angle > pi) {
    return angle - 2 * pi;
  }
  if (angle < -pi) {
    return angle + 2 * pi;
  }
  return angle;
}

void KalmanFilter::Init(Eigen::VectorXd const &X, Eigen::MatrixXd const &P) {
  X_ = X;
  P_ = P;
}

void KalmanFilter::Predict(Eigen::MatrixXd const &F, Eigen::MatrixXd const &Q) {
  // Update state X
  X_ = F * X_;
  // Update covariance matrix
  // NOTE: F describes a linear process, so the maths is simple now
  // To describe nonlinear  processes check this out:
  //    https://www.cse.sc.edu/~terejanu/files/tutorialEKF.pdf
  P_ = F * P_ * F.transpose() + Q;
}

void KalmanFilter::Update(Eigen::MatrixXd const &J, Eigen::MatrixXd const &R,
                          Eigen::VectorXd const &Z) {
  Eigen::VectorXd Y = Z - J * X_;
  updateState(J, R, Y);
}

void KalmanFilter::UpdateEKF(Eigen::MatrixXd const &J, Eigen::MatrixXd const &R,
                             Eigen::VectorXd const &Z) {
  const double eps = 0.00001;

  double x = X_(0);
  double y = X_(1);
  double vx = X_(2);
  double vy = X_(3);
  double r = std::sqrt(x * x + y * y);
  double phi;
  // Handling domain error case, see params section
  // http://www.cplusplus.com/reference/cmath/atan2/
  if (x + y < eps) {
      phi = 0.;
  } else {
      phi = std::atan2(y, x);
  }
  if (r < eps) {
    std::cerr << "r < eps!\n";
    r = eps;
  }
  double vr = (x * vx + y * vy) / r;

  Eigen::Vector3d Zpredicted;
  Zpredicted << r, phi, vr;

  Eigen::Vector3d Y = Z - Zpredicted; // can't use auto, it uses const then
  Y(1) = normalizeAngle(Y(1));

  updateState(J, R, Y);
}

void KalmanFilter::updateState(Eigen::MatrixXd const &J,
                               Eigen::MatrixXd const &R,
                               Eigen::VectorXd const &Y) {
  Eigen::MatrixXd Jt = J.transpose();
  Eigen::MatrixXd S = J * (P_ * Jt) + R;
  Eigen::MatrixXd K = (P_ * Jt) * S.inverse();
  Eigen::MatrixXd I = Eigen::MatrixXd::Identity(X_.size(), X_.size());

  X_ = X_ + K * Y;
  P_ = (I - K * J) * P_;
}
