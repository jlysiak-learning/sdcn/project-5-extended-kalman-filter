#ifndef TOOLS_H_
#define TOOLS_H_

#include "Eigen/Dense"
#include <vector>

namespace Tools {
Eigen::VectorXd CalculateRMSE(std::vector<Eigen::VectorXd> const &estimations,
                              std::vector<Eigen::VectorXd> const &ground_truth);

Eigen::MatrixXd CalculateJacobian(const Eigen::VectorXd &x);
} // namespace Tools

#endif // TOOLS_H_
