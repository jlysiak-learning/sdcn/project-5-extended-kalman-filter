#ifndef FusionEKF_H_
#define FusionEKF_H_

#include "Eigen/Dense"
#include "kalman_filter.h"
#include "measurement_package.h"
#include "tools.h"
#include <fstream>
#include <string>
#include <vector>

class FusionEKF {
public:
  FusionEKF();

  // Run the whole flow of the Kalman Filter from here.
  void ProcessMeasurement(MeasurementPackage const &measurement_pack);

  Eigen::VectorXd const &X() const { return ekf_.X(); }

private:
  // check whether the tracking toolbox was initialized or not (first
  // measurement)
  bool is_initialized_;

  // previous timestamp
  long long previous_timestamp_;

  // Measurement covariance matrices, const over time
  Eigen::MatrixXd R_laser_;
  Eigen::MatrixXd R_radar_;
  // Laser Jacobian, const over time
  Eigen::MatrixXd J_laser_;

  KalmanFilter ekf_;
};

#endif // FusionEKF_H_
