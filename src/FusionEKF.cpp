#include "FusionEKF.h"
#include "Eigen/Dense"
#include "tools.h"
#include <iostream>

FusionEKF::FusionEKF() {
  is_initialized_ = false;
  previous_timestamp_ = 0;
  // initializing matrices
  R_laser_ = Eigen::MatrixXd(2, 2);
  R_radar_ = Eigen::MatrixXd(3, 3);
  J_laser_ = Eigen::MatrixXd(2, 4);
  // measurement covariance matrix - laser
  R_laser_ << 0.0225, 0, 0, 0.0225;
  // measurement covariance matrix - radar
  R_radar_ << 0.09, 0, 0, 0, 0.0009, 0, 0, 0, 0.09;
  // Laser sensor matrix
  J_laser_ << 1, 0, 0, 0, 0, 1, 0, 0;
}

void FusionEKF::ProcessMeasurement(MeasurementPackage const &measurement_pack) {
  if (!is_initialized_) {
    // Initialize covariance matrix P
      Eigen::MatrixXd P = Eigen::MatrixXd(4, 4);
    // state
    Eigen::VectorXd X = Eigen::VectorXd(4);

    if (measurement_pack.sensor_type_ == MeasurementPackage::RADAR) {
      double r = measurement_pack.raw_measurements_[0];
      double phi = measurement_pack.raw_measurements_[1];
      X << r * cos(phi), r * sin(phi), 1, 1; // last vx, vy just guess
    } else {
      X << measurement_pack.raw_measurements_[0],
          measurement_pack.raw_measurements_[1], 1, 1;
    }
    // Initialize covariance matrix P
    // Huge variance --> high uncertainty
    P << 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1000, 0, 0, 0, 0, 1000;
    ekf_.Init(X, P);
    previous_timestamp_ = measurement_pack.timestamp_;
    // done initializing
    // after first measurement we don't predict or update
    is_initialized_ = true;
    // print the output
    std::cerr << "Initialized!" << std::endl;
    return;
  }
  /** Prediction */

  /** Update the state transition matrix F according to the new elapsed
   * time. Time is measured in seconds.  */
  long long timestamp = measurement_pack.timestamp_;
  // Delta time in seconds (convert from us)
  double dt = (timestamp - previous_timestamp_) / 1000000.;
  previous_timestamp_ = timestamp;
  Eigen::MatrixXd F = Eigen::MatrixXd(4, 4);
  F << 1, 0, dt, 0, 0, 1, 0, dt, 0, 0, 1, 0, 0, 0, 0, 1;

  /** Update the process noise covariance matrix. Use noise_ax = 9 and
   * noise_ay = 9 for Q matrix.  */
  double nax = 9;
  double nay = 9;
  double dt2 = dt * dt;
  double dt3 = dt2 * dt;
  double dt4 = dt2 * dt2;
  double q00 = 0.25 * nax * dt4;
  double q02 = 0.5 * nax * dt3;
  double q11 = 0.25 * nay * dt4;
  double q13 = 0.5 * nay * dt3;
  double q20 = q02;
  double q31 = q13;
  double q22 = dt2 * nax;
  double q33 = dt2 * nay;
  Eigen::MatrixXd Q = Eigen::MatrixXd(4, 4);
  Q << q00, 0, q02, 0, 0, q11, 0, q13, q20, 0, q22, 0, 0, q31, 0, q33;
  ekf_.Predict(F, Q);

  /** Update */
  if (measurement_pack.sensor_type_ == MeasurementPackage::RADAR) {
    Eigen::MatrixXd J = Tools::CalculateJacobian(ekf_.X());
    Eigen::MatrixXd R = R_radar_;
    ekf_.UpdateEKF(J, R, measurement_pack.raw_measurements_);
  } else if (measurement_pack.sensor_type_ == MeasurementPackage::LASER) {
    Eigen::MatrixXd J = J_laser_;
    Eigen::MatrixXd R = R_laser_;
    ekf_.Update(J, R, measurement_pack.raw_measurements_);
  }

  // print the output
  std::cout << "x_ = " << ekf_.X() << std::endl;
  std::cout << "P_ = " << ekf_.P() << std::endl;
}
