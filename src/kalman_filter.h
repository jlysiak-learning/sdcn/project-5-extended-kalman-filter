#ifndef KALMAN_FILTER_H_
#define KALMAN_FILTER_H_

#include "Eigen/Dense"

class KalmanFilter {
public:
  /** Initializes Kalman filter
   * @param x_in Initial state
   * @param P_in Initial state covariance */
  void Init(Eigen::VectorXd const &X, Eigen::MatrixXd const &P);

  /** Predict next state
   * @param F transitioni matrix (time dependent)
   * @param Q Process noise */
  void Predict(Eigen::MatrixXd const &F, Eigen::MatrixXd const &Q);

  /** Updates the state by using standard Kalman Filter equations
   * @param J Jacobian
   * @param R Measurement covariance matrix
   * @param Z The measurement at k+1 */
  void Update(Eigen::MatrixXd const &J, Eigen::MatrixXd const &R,
              Eigen::VectorXd const &Z);

  /** Updates the state by using Extended Kalman Filter equations
   * @param J Jacobian
   * @param R Measurement covariance matrix
   * @param z The measurement at k+1 */
  void UpdateEKF(Eigen::MatrixXd const &J, Eigen::MatrixXd const &R,
                 Eigen::VectorXd const &Z);

  Eigen::VectorXd const &X() const { return X_; }
  Eigen::MatrixXd const &P() const { return P_; }

private:
  void updateState(Eigen::MatrixXd const &J, Eigen::MatrixXd const &R,
                   Eigen::VectorXd const &Y);

  // state vector
  Eigen::VectorXd X_;

  // state covariance matrix
  Eigen::MatrixXd P_;
};

#endif // KALMAN_FILTER_H_
