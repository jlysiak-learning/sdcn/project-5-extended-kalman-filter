#include "tools.h"

#include <iostream>

namespace Tools {

Eigen::VectorXd
CalculateRMSE(std::vector<Eigen::VectorXd> const &estimations,
              std::vector<Eigen::VectorXd> const &ground_truth) {
  assert(estimations.size());
  assert(estimations.size() == ground_truth.size());

  Eigen::Vector4d rmse = Eigen::VectorXd::Zero(4);
  for (size_t i = 0; i < estimations.size(); ++i) {
    auto e = estimations[i];
    auto gt = ground_truth[i];
    auto diff = e - gt;
    auto prod = diff.array() * diff.array();
    rmse += prod.matrix();
  }
  rmse /= ground_truth.size();
  return rmse.array().sqrt();
}

Eigen::MatrixXd CalculateJacobian(Eigen::VectorXd const &X) {
  const double r_eps = 0.00001;
  double x = X(0);
  double y = X(1);
  double vx = X(2);
  double vy = X(3);
  double r = std::sqrt(x * x + y * y);
  if (r < r_eps) {
    std::cerr << "r < r_eps, reset to r_eps=" << r_eps << std::endl;
    r = r_eps;
  }

  Eigen::MatrixXd J(3, 4);

  double r_inv = 1. / r;
  double r_inv2 = r_inv * r_inv;
  double r_inv3 = r_inv2 * r_inv;
  double vx_y = vx * y;
  double vy_x = vy * x;
  double vy_x_vx_y = vy_x - vx_y;
  double b = vy_x_vx_y * r_inv3; // sorry no idea how to name it better :)

  double h00 = x * r_inv;
  double h01 = y * r_inv;
  double h10 = -y * r_inv2;
  double h11 = x * r_inv2;
  double h20 = -y * b;
  double h21 = x * b;
  J << h00, h01, 0, 0, h10, h11, 0, 0, h20, h21, h00, h01;

  return J;
}

} // namespace Tools
