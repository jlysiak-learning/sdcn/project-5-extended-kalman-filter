# Extended Kalman Filter - Self-Driving Cars Nanodegree project

## Directory contents

- `src`: EKF code
- `udacity-misc`: some miscellaneous stuff from the starter
  [repo](https://github.com/udacity/CarND-Extended-Kalman-Filter-Project).
- `install-linux.sh`: installs required tools, like cmake, gcc, libssl, etc.
  Also, downloads `uWebSockets` from github, builds it and copies important
  files: `libuWS.so` and include files.

## Building the app

Run: `mkdir build && cmake .. && make`

A lot of warnings come from `Eigen` lib compilation, so don't worry. :)

Now you can run `./ExtendedEKF` and [Term2](https://github.com/udacity/self-driving-car-sim/releases) sim.

## Results

Extended Kalman Filter is initialized when the first measurement is received (`src/FusionEKF.cpp:28`).
Initial values are:
- state vector `X`:
    * `x`: measured `x`
    * `y`: measured `y`
    * `vx`: 1. (just guess)
    * `vy`: 1. (just guess)
- covariance matrix `P`:
    * `0,0`: 1.
    * `l,1`: 1.
    * `2,2`: 1000.
    * `3,3`: 1000.
1 and 1000 are arbitrary, we express relative certainty. Basically, we DON'T
know the velocity, so the uncertainty is large.

Results on `dataset1`:
* RMSE x: 0.0965
* RMSE y: 0.0854
* RMSE vx: 0.4158
* RMSE vy: 0.4321
![img](./images/dataset1.png)

Results on `dataset2:`:
* RMSE x: 0.0727
* RMSE y: 0.0969
* RMSE vx: 0.4897
* RMSE vy: 0.5083
![img](./images/dataset2.png)


## TODOs

- Investigate why using `auto` instead of `Eigen::MatrixXd` leaded to
    zeroed matrices even if all elements of matrix multiplication were not zero.
    It was strange a strate error.
